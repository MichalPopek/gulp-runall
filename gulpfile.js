const path = require('path');
const hub = require('gulp-hub');
const glob = require('glob');

const root = path.join(__dirname, 'test');
const gulpfiles = glob
    .sync(`${root}/**/gulpfile.js`)
    .map(gulpfile => path.relative(__dirname, gulpfile));

hub(gulpfiles);